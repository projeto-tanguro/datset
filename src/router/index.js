import Vue from 'vue'
import VueRouter from 'vue-router'
        Vue.use(VueRouter)
import Home from '../view/ds-home.vue';
import NotFoundComponent from '../view/404.vue';

const routes = [
    {path: '/category/:info/id/:id', component: Home,meta: {title: 'Datset'}},
    {path: '/', component: Home, props: {'info':'project'},meta: {title: 'Datset Home'}},
    { path: '*', component: NotFoundComponent, meta: {title: 'Datset 404'} }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
export default new VueRouter({
    mode: 'history',
    routes: routes
})

