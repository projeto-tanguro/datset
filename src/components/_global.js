/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import Vue from 'vue'
import {upperFirst, camelCase} from 'lodash'

// Busca todos os componentes na pasta ./components
const requireComponent = require.context(
  '.', false, /[\w-]+\.vue$/ 
)

requireComponent.keys().forEach( fileName => {
  // Pega o componente
  const componentConfig = requireComponent(fileName)

  // Transforma o nome em PascalCase
  const componentName = upperFirst(
    camelCase( fileName.replace(/^\.\//, '').replace(/\.\w+$/, '') )
  )

  // Registra globalmente o componente
  Vue.component( componentName, componentConfig.default || componentConfig )
})