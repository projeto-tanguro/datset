import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App.vue'; 
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import "./assets/styles/blog.css"

import './components/_global'

window.axios = require('axios');
Vue.prototype.$http = window.axios
Vue.use(BootstrapVue)

import router from './router'


new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
